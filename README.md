[TOC]



# 解决方案介绍

该解决方案基于华为云语音交互服务的录音文件识别技术构建，可以自动对用户上传到对象存储服务的客服中心录音进行语音质检，从海量录音的质检结果中挖掘业务中的不礼貌服务用语、引导客户投诉等违规风险项，并及时进行处理，保证客服业务合规性。

解决方案实践详情页面：https://www.huaweicloud.com/solution/implementations/speech-recognition-call-center-speech-analytics.html

## 架构图

![](./document/speech-recognition-call-center-speech-analytics.png)

## 架构描述

该解决方案会部署如下资源：

1. 创建两个OBS桶，一个提供给用户上传隐私通话录音，另一个用于转储客服中心录音质检结果JSON文件；
2. 在函数工作流中创建函数，当OBS桶收到上传的客服中心录音后，会自动触发函数调用语音交互服务，获取录音转写结果后进行语音内容质检，并上传语音转写结果和质检结果。
3. 语音交互服务，将客服中心语音文件转化为文字。

## 组织结构

```
huaweicloud-solution-speech-recognition-call-center-speech-analytics
├── speech-recognition-call-center-speech-analytics.tf.json -- 资源编排模板
├── functiongraph
    ├── index.py  -- 函数入口文件
    ├── sa_utils.py -- 对话分析工具包
    ├── sis_utils.py -- 语音识别工具包
```

## 开始使用

1、 登陆华为云[对象存储服务](https://console.huaweicloud.com/console/?region=cn-north-4&locale=zh-cn#/obs/manager/buckets)控制台，查看OBS桶是否正常创建。

图1 查看OBS桶

![](./document/readme-image-001.PNG)

2、选择桶`call-center-speech-analytics-demo-input` （以默认参数为例，实际桶名称以部署指定参数为准），上传客服中心通话录音文件（上传客服中心录音样例`call-center-speech-analytics-demo.wav`）。

图2 上传客服中心录音文件

![](./document/readme-image-002.PNG)

3、 选择桶`call-center-speech-analytics-demo-output`（以默认参数为例，实际桶名称以部署指定参数为准），查看客服中心质检结果JSON文件。

图3 客服中心质检结果JSON文件

![](./document/readme-image-003.PNG)

