# -*- coding:utf-8 -*-
"""
SolutionAsCode-客服中心语音质检

示例： 客服中心录音文件中是否出现投诉、升级话术

"""


# -*-coding:utf-8 -*-
import os
import sys
import json
import urllib
import requests
import time

from sis_utils import build_obs_client, download_file_from_obs, upload_file_to_obs, get_headers, \
    get_norm_result, get_post_url, get_data_url

from sa_utils import analysis_speech

requests.packages.urllib3.disable_warnings()

current_file_path = os.path.dirname(os.path.realpath(__file__))
sys.path.append(current_file_path)


def get_payload(data_url, channel) :
    """ 获取API入参
    """
    config =  {
		"audio_format": "auto",
		"property": "chinese_8k_general",
		"add_punc": "yes",
		"need_analysis_info": {
			"diarization": True,
            "channel": channel,
			"emotion": True,
			"speed": True
		}
	
    }
    payload = {
        "data_url" :data_url,
        "config" : config
    }
    print("数据%s"%payload)
    return payload


def handler(event, context) :

    # 获取环境变量，需提前设置
    region = os.getenv('region')

    # dynamic_source是event中一个特殊结构体，通过它算子提供方可以获取用户配置的参数
    input_bucket_name = urllib.parse.unquote(event['Records'][0]['s3']['bucket']['name'], encoding='utf-8')
    input_object_name = urllib.parse.unquote(event['Records'][0]['s3']['object']['key'], encoding='utf-8')

    channel = context.getUserData('channel', "LEFT_AGENT")

    # 向API发送请求
    data_url = get_data_url(region, input_bucket_name, input_object_name)
    payload = get_payload(data_url, channel)
    headers = get_headers(context)
    post_url = get_post_url(context, region)
    response = requests.post(post_url, headers=headers, json=payload, verify=False, timeout=20)
    if response.status_code != 200 :
        raise Exception(response.text)
    
    # 获取客服中心录音转写结果
    job_id = json.loads(response.text).get("job_id")
    status = "WAITING"
    #最多等待900秒,15min
    count = 180
    while status != "FINISHED" and count > 0:
        # 轮询job_id,状态为FINISHED或超过等待时间跳出循环
        response = requests.get(post_url+"/"+job_id,headers=headers, verify=False, timeout=20)
        if response.status_code != 200 :
            raise Exception(response.text)
        status = json.loads(response.text).get("status")
        time.sleep(5)
        count-=1
    # 如果等待时间超过15min,则返回音频太长提示
    if status == "WAITING":
        raise Exception("audio too long:"+ job_id)

    print(response.text)


    # 获取请求结果
    asr_res = json.loads(response.text)

    # 分析语音识别结果，并给出质检详情
    response_data = analysis_speech(asr_res, data_url)

    # 将音频字节码上传到目标桶,目标桶必须存在,且最好和事件源桶不一样，避免再触发一次
    result_bucket = context.getUserData('result_bucket')
    if not result_bucket:
        raise Exception("result_bucket is empty")
    obs_client = build_obs_client(context, region)
    remote_audio_object = os.path.splitext(os.path.basename(input_object_name))[0]  + ".json"
    upload_file_to_obs(obs_client, result_bucket, remote_audio_object, response_data)
    obs_client.close()



    # 将质检结果封装成functiongraph格式结果
    return get_norm_result(event, response_data) 
